// Datos de color por p�xel que han pasado por el sombreador de p�xeles.
struct PixelShaderInput
{
	float4 pos : SV_POSITION;
	float3 color : COLOR0;
};

// Funci�n de paso por los datos de color (interpolado).
float4 main(PixelShaderInput input) : SV_TARGET
{
	return float4(input.color, 1.0f);
}
