// B�fer de constantes que almacena las tres matrices principales de columna b�sicas para componer la geometr�a.
cbuffer ModelViewProjectionConstantBuffer : register(b0)
{
	matrix model;
	matrix view;
	matrix projection;
};

// Datos de v�rtice usados como entrada en el sombreador de v�rtices.
struct VertexShaderInput
{
	float3 pos : POSITION;
	float3 color : COLOR0;
};

// Datos de color por p�xel que han pasado por el sombreador de p�xeles.
struct PixelShaderInput
{
	float4 pos : SV_POSITION;
	float3 color : COLOR0;
};

// Sombreador sencillo para el procesamiento de v�rtices en la GPU.
PixelShaderInput main(VertexShaderInput input)
{
	PixelShaderInput output;
	float4 pos = float4(input.pos, 1.0f);

	// Transformar la posici�n del v�rtice en espacio proyectado.
	pos = mul(pos, model);
	pos = mul(pos, view);
	pos = mul(pos, projection);
	output.pos = pos;

	// Pasar el color sin modificaci�n.
	output.color = input.color;

	return output;
}
