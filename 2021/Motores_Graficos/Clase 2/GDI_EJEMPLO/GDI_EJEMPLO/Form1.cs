﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GDI_EJEMPLO
{
    public partial class Form1 : Form
    {

        Graphics graficador;

        Brush brocha;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            graficador = this.CreateGraphics() ;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            graficador.Clear(Color.Blue);

            PintarRectangulo();
        }

        void PintarRectangulo()
        {
            Rectangle rectangulo = new Rectangle(10, 10, 100, 200);

            if (radioButton1.Checked)
            {

                brocha = Brushes.White;
            }
            else if (radioButton2.Checked)
            {
                brocha = Brushes.Yellow;
            }
            else
            {
                brocha = Brushes.Red;
            }


            graficador.FillRectangle(brocha, rectangulo);

            Pen lapiz = new Pen(Brushes.Black, 5);

            graficador.DrawRectangle(lapiz, rectangulo);


           
        }

    }
}
