﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GDI_NAVES
{
    public partial class Form1 : Form, IJugable
    {

        Graphics graficador;

        Graphics Graficador2; 

        Fondo fondo;

        Nave player;

        List<Disparo> disparos = new List<Disparo>();

        //1230; 924

        Bitmap bmp = new Bitmap(1230, 924);
        Image render;


        public Form1()
        {
            InitializeComponent();
        }
        //ACÁ ACTUALIZO TODOS LOS SPRITES
        public void Actualizar()
        {
            foreach(Disparo d in disparos)
            {
                d.Actualizar();
            }
        }
        //ACÁ DIBUJO TODOS LOS SPRITES
        public void Dibujar()
        {
            fondo.Dibujar();

            player.Dibujar();

            foreach (Disparo d in disparos)
            {
                d.Dibujar();
            }

            Graficador2.DrawImage(render, 0, 0);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
         //   this.Text = DateTime.Now.ToString();

            Actualizar();
            Dibujar();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            render = Image.FromHbitmap(bmp.GetHbitmap());

            graficador = Graphics.FromImage(render);
                 
            Graficador2 = this.CreateGraphics();



            fondo = new Fondo(graficador);

            fondo.Imagen = @"Imagenes\mars.jpg";

            player = new Nave(graficador);

            player.Imagen = @"Imagenes\Nave_1.png";

            player.Ubicacion = new Point(this.Size.Width / 2, 500);

        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.ToString() == "a")
            {
                //Mover Izq
                player.Mover(-10);
            }
            else if (e.KeyChar.ToString() == "d")
            {
                //Mover Der
                player.Mover(10);
            }


            if (e.KeyChar.ToString() == "l")
            {
                Disparo d = player.Disparar();

                if (d !=null)
                { 
                    disparos.Add( d );
                }
            }
        }
    }
}
