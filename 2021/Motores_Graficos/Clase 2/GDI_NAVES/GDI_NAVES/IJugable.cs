﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDI_NAVES
{
    interface IJugable
    {
        void Actualizar();
        void Dibujar();
    }
}
