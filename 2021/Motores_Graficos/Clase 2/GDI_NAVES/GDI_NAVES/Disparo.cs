﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
namespace GDI_NAVES
{
    class Disparo: Sprite, IJugable
    {
        public Disparo(Graphics g) : base(g)
        {
        }

        private Point ubicacion;

        public Point Ubicacion
        {
            get { return ubicacion; }
            set { ubicacion = value; }
        }

        private int velocidad;

        public int Velocidad
        {
            get { return velocidad; }
            set { velocidad = value; }
        }


        public void Actualizar()
        {
            ubicacion = new Point(ubicacion.X, ubicacion.Y - velocidad);
        }

        public void Dibujar()
        {
            graficador.DrawImage(textura, ubicacion.X, ubicacion.Y, 20, 70);
        }
    }
}
