﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GDI_NAVES
{
    class Nave :Sprite, IJugable
    {
        private DateTime ultimoDisparo = new DateTime();

        private Point ubicacion;

        public Point Ubicacion
        {
            get { return ubicacion; }
            set { ubicacion = value; }
        }

        private bool vivo =true;

        public bool Vivo
        {
            get { return vivo; }
            set { vivo = value; }
        }




        public Nave(Graphics g) : base(g)
        {
        }


        public void Mover(int x)
        {

            ubicacion = new Point(ubicacion.X + x, ubicacion.Y);
        }


        public void Actualizar()
        {
          
        }

        public void Dibujar()
        {

            graficador.DrawImage(textura, ubicacion.X,ubicacion.Y,50,100 );

        }

        public Disparo Disparar()
        {
            Disparo disparo = null;

            if( (DateTime.Now - ultimoDisparo).TotalMilliseconds > 500  )
            { 
                disparo = new Disparo(graficador);
                disparo.Imagen = @"Imagenes\Disparo.png";
                disparo.Ubicacion = this.ubicacion;
                disparo.Velocidad = 10;
                ultimoDisparo = DateTime.Now;
            }
            return disparo;
        }

        public bool DetectarColision(Disparo disparo)
        {
            //Comparo si el disparo está tocando o no
            return true;
        }
    }
}
