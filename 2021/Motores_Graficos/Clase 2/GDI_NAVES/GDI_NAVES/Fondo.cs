﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GDI_NAVES
{
    class Fondo :Sprite, IJugable
    {
        public Fondo(Graphics g):base(g)
        { }

        public void Actualizar()
        {
            throw new NotImplementedException();
        }

        public void Dibujar()
        {
            Point coordenada = new Point(0, 0);

            graficador.DrawImage(textura,coordenada);
        }
    }
}
