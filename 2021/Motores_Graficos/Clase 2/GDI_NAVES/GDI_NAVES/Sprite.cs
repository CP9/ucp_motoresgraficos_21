﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GDI_NAVES
{
    abstract class Sprite
    {
        protected Graphics graficador;

        protected Image textura;

        public Sprite(Graphics g)
        {
            graficador = g;
        }


        protected string imagen;

        public string Imagen
        {
            get { return imagen; }
            set
            {
                imagen = value;
                LeerImagen();
            }
        }

        private void LeerImagen()
        {
            textura = Image.FromFile(imagen);
        }


    }
}
