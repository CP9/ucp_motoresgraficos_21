﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Llegada : MonoBehaviour
{
    
    public Movimiento player;
    public Record record;
    // Start is called before the first frame update
    void Start()
    {
     record = FindObjectOfType<Record>();   
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider o )
    {
        if (player.tiempotranscurrido.TotalMilliseconds < record.MejorTiempo.TotalMilliseconds)
        {
            record.MejorTiempo = player.tiempotranscurrido;
            Pantalla p = FindObjectOfType<Pantalla>();
            p.IrMenu();
        }


    }

}
