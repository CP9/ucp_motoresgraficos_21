﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
public class Record : MonoBehaviour
{
    public Text txtPuntos;

    public Text txtTiempo;
    
    public TimeSpan MejorTiempo ;


    public long Puntos;
    // Start is called before the first frame update
    void Start()
    {
        Record[] objetos = GameObject.FindObjectsOfType<Record>();

        if(objetos.Length >1)
        {
            Destroy(this.gameObject);
        }
        else
        {
            MejorTiempo = DateTime.Now -( new DateTime());
        }
        DontDestroyOnLoad(this.gameObject);


    }

    // Update is called once per frame
    void Update()
    {
        if(txtPuntos == null)
        {
            GameObject p = GameObject.Find("txtPuntos");
         
            if(p !=null)
            {
                txtPuntos  =p.GetComponent<Text>();
            }
        }
        if(txtTiempo ==null)
        {
            GameObject t = GameObject.Find("txtTiempo");
            if(t != null)
            {
                txtTiempo =   t.GetComponent<Text>();
            }
        }
        if(txtPuntos!=null)
        {
            txtPuntos.text = Puntos.ToString(); 
        }


        if(txtTiempo !=null)
        {
            txtTiempo.text = MejorTiempo.ToString();

        }

    }
}
