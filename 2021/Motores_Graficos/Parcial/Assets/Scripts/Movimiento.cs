﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class Movimiento : MonoBehaviour
{
    public Text txtTiempo;
    public TimeSpan tiempotranscurrido;
    private DateTime comienzo;

    public float vel = 0.5f;

    public float velRot = 10f;
    // Start is called before the first frame update
    void Start()
    {
        comienzo = DateTime.Now;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = new Vector3(0f,0f,  Input.GetAxis("Vertical") * vel * Time.deltaTime   );

        this.transform.Translate(pos, Space.Self);
        
        Vector3 rot = new Vector3(0f, Input.GetAxis("Horizontal") * velRot * Time.deltaTime  ,0f);
        this.transform.Rotate( rot );

        if(Input.GetAxis("r") != 0 )
        {
             Pantalla p = FindObjectOfType<Pantalla>();
             p.IrPantalla();

        }
        if(Input.GetAxis("q") != 0 )
        {
             Pantalla p = FindObjectOfType<Pantalla>();
             p.IrMenu();

        }

        tiempotranscurrido = DateTime.Now - comienzo;
        txtTiempo.text  = tiempotranscurrido.ToString();
    }
}
