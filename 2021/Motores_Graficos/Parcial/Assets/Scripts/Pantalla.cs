﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pantalla : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Pantalla[] objetos = GameObject.FindObjectsOfType<Pantalla>();

        if(objetos.Length >1)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
    }

    public void IrMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void IrPantalla()
    {
        SceneManager.LoadScene(1);

    } 
    // Update is called once per frame
    void Update()
    {
     
    }
}
